clear, clc, close all
fprintf('Starting...\n')
%% 1ST PART OF PROGRAM: ORIGIN
%% Define the region. This is my window of viewing.
p     = 100;            % Pixels between two integers in final image
y     = [-2: 1/p: 2];
x     = [-2: 1/p: 2];
n     = numel(y);       % Handle for vector-length
[X,Y] = meshgrid(x,y);  % Produces a 2-D grid that's going to be transformed to the complex plane
Z     = X + 1i*Y;       % The complex matrix representing constants and the plane
z     = zeros(n,n);     % The complex matrix representing the origin (used to generate the Mandelbrot-set)
%% Setting up some constants. 
depth           = 20;           %input('Enter the iteration "depth" for origin-fractal: '); % Only for the Mandel-loop
how_many_images = 1;            %input('Enter "how_many_images" for origin-fractal: ');     % Only for the Mandel-loop
nloop           = -2;           % How many times does the movie loop? (- = both -> & <-,+ = only ->)
fps             = 15;           % With what frame-rate does the movie play?
%% Calling the mandelit loop to produce origin-image(s).
tic
OriginFract = mandelit(z,Z,how_many_images,depth);      % mandelit.m performs the mathurements on the entire constant plain and records what the origin does
toc
%% Display origin-image(s).
figure(1)
mandelplot(OriginFract,how_many_images,depth,nloop,fps) % This plots the image(s) of what the origin does under all complex transformations using mandelit.m
fprintf('Done with Origin fractal, proceeding... \n\n')

%% 2ND PART OF PROGRAM: JULIA
%% Get input for Julia creation.
depth                = 30;                                   %input('Enter the iteration "depth" for JULIA-fractal: ');  % Only for the JULIA-loop
how_many_points      = 10;                                   %input('Enter "how_many_points" for JULIA-fractal: ');      % Only for the JULIA-loop (points per vector)
how_many_clicks      = 8;                                    % How many vectorial endpoints do you wish to create?
how_many_images      = (how_many_clicks - 1)*how_many_points;% Change how many images will be computed based on clicks in image (vectors produced)

JuliaVect            = zeros(1,how_many_images)';            % Preallocation for speed
fprintf('Click %d times on the image to produce vectorial endpoints \n', how_many_clicks)
[Real,Imaginary]     = ginput(how_many_clicks);              % Expects how_many_clicks number of mouse-clicks from the last produced figure
fprintf('Compiling coordinates from clicks as inputs to Julia-formation...')
RE                   = (Real      - (x(n) - x(1))/2*p)/p;    % Resizing real + transform origin to be in the middle of image
IMAG                 = (Imaginary - (y(n) - y(1))/2*p)/p*i;  % Resizing imaginary + same as above
for i = 1:how_many_clicks - 1                                % Creating how_many_clicks - 1 "vectors" (contained in one vector)
    JuliaVect(how_many_points*(i-1)+1:how_many_points*i) = linspace(RE(i),RE(i+1),how_many_points) + linspace(IMAG(i),IMAG(i+1),how_many_points); % Produce vector with Julia-numbers
end
%% Calling the mandelit loop to produce JULIA-image(s).
tic
CFract = mandelit(Z,JuliaVect,how_many_images,depth);   % Same as previous use, only this time it uses the entire Cplane and the vector from mouse-cliks. 
toc
%% Display JULIA-image(s).

figure(2)
mandelplot(CFract,how_many_images,depth,nloop,fps)      % Same as previous use, only this time it plots the Julia images
fprintf('Done with Julia fractal. Wish I could see it... Remember to change the name: VideoWriter(Nomalos_XX.avi) in mandelplot.m before next run \n')