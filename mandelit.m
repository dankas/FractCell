function [ Fractalbum ] = mandelit( plane, constant, how_many_images, depth )
%% Here is the Mandelbrot iteration, generalized for any function. 
temp_plane    = plane;                        % Saved for later use
temp_constant = constant;                     % Saved for later use
dimension     = size(constant);               % Conditions within loop
for j = 1:how_many_images  
  image = zeros(length(plane),length(plane)); % Making sure that the canvas is empty before every new painting so to speak
  plane = temp_plane;                         % Reloading whatever plane we're iterating upon
  if dimension(2) == 1                        % If true -> constant = vector of JULIA-numbers
      constant = temp_constant(j);            % Adjust what is looped  
  end
  for k = 1:depth;
      plane                 = sinh(plane) - cosh(constant) + constant;    % Whatever equation you want, compute to your graphic cards content
      image(abs(plane) < 2) = k;                                          % If magnitude of entry in image is < 2, store k -> increase k. 
  end
Fractalbum{j} = image;                                                    % All the images
end
fprintf('Done with calculation, proceeding... \n')